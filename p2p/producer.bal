import ballerina/http;
import ballerinax/kafka;
import ballerina/io;
import ballerina/'lang\.float as langfloat;

// Variables to store adminstrators credentials
final string ADMIN_USERNAME = "Admin";
final string ADMIN_PASSWORD = "Admin";

//here we create producer configurations with optional parameters
kafka:ProducerConfig producerConfigs = {
    bootstrapServers: "localhost:9092",
    clientID: "producer",
    acks: "all",
    noRetries: 3,
	autoCommit: false
};

kafka:Producer kafkaProducer = new(producerConfigs);

// kafka service endpoint
listener kafka:Listener kafkaListener = new(9092);

@kafka:ServiceConfig { basePath: "/" }
service kafkaService on kafkaListener {


	remote function onConsumerRecord(kafka:Caller caller, kafka:ConsumerRecord[] records) returns error? {
        foreach var kafkaRecord in records {

			kafka:Response response = new;
			float newPriceAmount = 0.0;
			json|error reqPayload = request.getJsonPayload();

			if (reqPayload is error) {
				response.statusCode = 400;
				response.setJsonPayload({ "Message": "Entered payload is incorrect." });
				var result = caller->respond(response);
			} else {
				json|error username = reqPayload.Username;
				json|error password = reqPayload.Password;
				json|error productName = reqPayload.Product;
				json|error newPrice = reqPayload.Price;

				// If payload parsing fails, send a "Bad Request" message as the response
				if (username is error || password is error || productName is error || newPrice is error) {
					response.statusCode = 400;
					response.setJsonPayload({ "Message": "You have entered incorrect information." });
					var responseResult = caller->respond(response);
				} else {
			// Convert the price value to float
			var result = langfloat:fromString(newPrice.toString());
			if (result is error) {
				response.statusCode = 400;
				response.setJsonPayload({ "Message": "Invalid amount specified" });
				var responseResult = caller->respond(response);
			} else {
				newPrice = result;
			}


			//if the entered credentials are a mismatch with the admistrator ones, a access forbidden is send here
			if (username.toString() != ADMIN_USERNAME || password.toString() != ADMIN_PASSWORD) {
				response.statusCode = 403;
				response.setJsonPayload({ "Message": "You have entered the wrong information, access is denied!" });
				var responseResult = caller->respond(response);
			}

			// Construct and serialize the message to be published to the Kafka topic
			json priceUpdateInfo = { "Product": productName, "UpdatedPrice": check newPrice };
			byte[] serializedMsg = priceUpdateInfo.toString().toBytes();

			// Produce the message and publish it to the Kafka topic
			var publishedMsg = kafkaProducer->send(serializedMsg, "product-price", partition = 0);
			// Send internal server error if the sending has failed
			if (publishedMsg is error) {
				response.statusCode = 500;
				response.setJsonPayload({ "Message": "Sending data hS failed." });
				var responseResult = caller->respond(response);
			}
			// Send a success status to the admin request
			response.setJsonPayload({ "Status": "Success" });
			var responseResult = caller->respond(response);
			}
			}
		}
    }
}


