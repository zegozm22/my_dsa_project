import ballerina/io;
import ballerinax/kafka;
import ballerina/log;

// Kafka consumer listener configurations
kafka:ConsumerConfig consumerConfigs = {
    bootstrapServers: "localhost:9092, localhost:9093",
    // Consumer group ID
    groupId: "instance1",
    // Listen from assignment2 topic
    topics: ["assignment2"],
    // Poll every 1 second
    pollingIntervalInMillis: 1000
};

// Create kafka listener
listener kafka:simpleConsumer consumer = new(consumerConfig);

// Kafka service that listens from the topic 'product-price'
// priceUpdateservice subscribed to new product price updates from
// the product admin and updates the Database.
service kafkaService on consumer {
    // Triggered whenever a message added to the subscribed topic
    resource function onMessage(kafka:Consumer simpleConsumer, kafka:ConsumerRecord[] records) {

        foreach var entry in records {
            byte[] serializedMsg = entry.value;

            // Convert the serialized message to string message
            string msg = encoding:byteArrayToString(serializedMsg);
            io:println("New message received from the product admin");

            //the retrieved Kafka record is logged here
            io:println("Topic: " + entry.topic + "; Received Message: " + msg);
            
            io:println("Aknowledgement from instance 1.");
        }
    }
}

